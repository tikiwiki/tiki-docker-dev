FROM php:8.3-apache
LABEL maintainer "TikiWiki <tikiwiki-devel@lists.sourceforge.net>"

ARG WWW_UID
ARG WWW_GID
ARG WWW_UNAME
RUN groupadd -g ${WWW_GID} -o ${WWW_UNAME}
RUN useradd -m -u ${WWW_UID} -g ${WWW_GID} -o -s /bin/bash ${WWW_UNAME}

# Installing dependencies
RUN apt-get update \
    && apt-get install -y \
        libfreetype6-dev \
        libicu-dev \
        libjpeg-dev \
        libldap2-dev \
        libldb-dev \
#        libmemcached-dev \
        libonig-dev \
        libpng++-dev \
        libzip-dev \
        unzip \
        zlib1g-dev \
        libbz2-dev \
        ssmtp

# Installing NodeJS
RUN set -uex; \
    apt-get update; \
    apt-get install -y ca-certificates curl gnupg; \
    mkdir -p /etc/apt/keyrings; \
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key \
     | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg; \
    NODE_MAJOR=20; \
    echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" \
     > /etc/apt/sources.list.d/nodesource.list; \
    apt-get -qy update; \
    apt-get -qy install nodejs;

RUN ln -s /usr/lib/x86_64-linux-gnu/libldap.so /usr/lib/libldap.so \
    && ln -s /usr/lib/x86_64-linux-gnu/liblber.so /usr/lib/liblber.so \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install bcmath calendar gd intl ldap mysqli mbstring pdo_mysql zip bz2 \
    && printf "yes\n" | pecl install xdebug-3.3.2 \
    && printf "no\n"  | pecl install apcu-beta \
#    && printf "no\n"  | pecl install memcached \
    && echo 'extension=apcu.so' > /usr/local/etc/php/conf.d/pecl-apcu.ini \
#   && echo 'extension=memcached.so' > /usr/local/etc/php/conf.d/pecl-memcached.ini \
    && echo "extension=ldap.so" > /usr/local/etc/php/conf.d/docker-php-ext-ldap.ini

RUN rm -rf /tmp/* \
    && { \
        echo "file_uploads = On"; \
        echo "upload_max_filesize = 2048M"; \
        echo "post_max_size = 2048M"; \
        echo "max_file_uploads = 20"; \
    } > /usr/local/etc/php/conf.d/docker-uploads.ini

# Note: no need to install composer, as tiki setup.sh will download it.
#   But maybe could be usefull?
# RUN mkdir -p /var/www/.composer /var/www/.config \
#     && chown www-data:www-data /var/www/.composer /var/www/.config \
#     && curl -s -L -o /usr/local/bin/composer https://getcomposer.org/download/latest-1.x/composer.phar \
#     && chmod 755 /usr/local/bin/composer \
#     && { \
#         COMPOSER_HOME=/usr/local/share/composer \
#         COMPOSER_BIN_DIR=/usr/local/bin \
#         COMPOSER_CACHE_DIR="/tmp/composer" \
#         composer global require psy/psysh; \
#     } \
#     && rm -rf /tmp/*

COPY root/ /


CMD ["psysh"]

WORKDIR "/var/www/html"

RUN mkdir /var/www/sessions && chown ${WWW_UID}:${WWW_GID} /var/www/sessions
RUN echo "session.save_path=/var/www/sessions" \
    > /usr/local/etc/php/conf.d/tiki_session.ini 

RUN echo "mailhub=mail:1025" > /etc/ssmtp/ssmtp.conf

# VOLUME ["/var/www/conf.d/","/var/www/html/files/","/var/www/html/img/trackers/","/var/www/html/img/wiki_up/","/var/www/html/img/wiki/","/var/www/html/modules/cache/","/var/www/html/storage/","/var/www/html/temp/","/var/www/sessions/"]

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]

CMD ["apache2-foreground"]