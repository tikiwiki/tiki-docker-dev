Docker Development environment for Tiki
==========================================

> WARNING: this is a work in progress, only use if you know what you are doing.

This repo is intended to be cloned alongside a Tiki repo where you develop code and want to see how it fares.
Using this repo, you can optionaly avoid installing any dependency (PHP, NodeJS, ...) on your host computer.

> IMPORTANT NOTE: the Tiki build process is not done automagically in this setup. You must build Tiki using `setup.sh` (or equivalent).
> If you don't have the required software on your host (PHP, NodeJS, ...), you can build Tiki from inside the container.

Quick start
-------------

Create a `.env` with required information (see later, or just run the following command):

    printf "WWW_UID=`id -u`\nWWW_GID=`id -g`\nWWW_UNAME=`id -u -n`\n" > .env

Then you must build Tiki (using `setup.sh` for example, see later for a detailed explanation).

Then:

    docker-compose up

Then reach http://localhost:8088 in your browser.

Setup
-------

The `docker-compose.yml` supposes that your Tiki repo is in `../tiki`.

Create a `.env` file with following info about your current system user:

```
WWW_UID=1000
WWW_GID=1000
WWW_UNAME=john
```

This could be done with following command:

    printf "WWW_UID=`id -u`\nWWW_GID=`id -g`\nWWW_UNAME=`id -u -n`\n" > .env

Then, you need to build Tiki, using `sh setup.sh build` (or equivalent), and eventually `php console.php dev:configure`.
If you have PHP and NodeJS on your host, you can do this directly on your host.
Else, you can do it in the `tiki` container:

    # launch a bash in the container:
    docker-compose exec tiki bash
    # then, from this bash;
    sh setup.sh build
    # TODO: add a test DB for the following to work.
    php console.php dev:configure

Once Tiki is build, you can reach http://localhost:8088 in your browser, and follow the installation steps.

> Note: the first time, the installer will ask you for the database credentials. These are in the `root/local.php` file.

> For now, the SMTP is not autoconfigured. You have to go to general preferences, enable the advanced features, and configure mail to be sent using SMTP (server: `tiki_mail`, port: `1025`). Sent mail will be available in mailpit.

Usage
-----------

When your docker-compose is up, you can reach

| App        | URL                   | or (using [dinghy-http-proxy](https://hub.docker.com/r/codekitchen/dinghy-http-proxy/))      |
|------------|-----------------------|-------------------------------|
| tiki       | http://localhost:8088 | http://tiki.docker            |
| phpmyadmin | http://localhost:8080 | http://phpmyadmin.tiki.docker |
| mailpit    | http://localhost:8025 | http://mail.tiki.docker       |

If you need to execute commands on the Tiki container:

    docker-compose exec tiki php console.php

Or for just having a shell there:

    docker-compose exec tiki bash

Sometimes you may want to reset the db, the quickest way is:

    docker-compose down
    sudo rm -rf data
    docker-compose up

Unit tests
-------

The docker-compose file includes a test database.

You can run the unit test in the container as following:

    # launch a bash in the container:
    docker-compose exec tiki bash
    php phpunit

TODO
-------

* make xDebug work
* handle custom htaccess/apache config
* add various seed sql dumps for quick population of the database
* add more docker-compose recipes (alternative `docker-compose.yml` files) for various cases
  * different setup with php-fpm and nginx proxy
  * ldap auth
  * oauth auth
  * double-tiki for testing intertiki data and auth sharing
  * different db backends
* etc ...(suggest more, if you think of any)

Maintainers and Contributors
------------------------------

* mose
* John Livingston

Copyright
--------------

Go to hell copyright. Content of this repository is public domain.
