#!/bin/sh

# If the following dir does not exist, there is a bug in setup.sh:
#   it won't install dev dependencies.
#   As we are in a dev environment, we want these dependencies to be installed.
mkdir -p /var/www/html/vendor_bundled/vendor/phpunit
