#!/bin/sh

/bin/bash htaccess.sh

mkdir -p /var/www/sessions
mkdir -p /var/www/html/dump
chown -R `id -u -n` /var/www/sessions

mkdir -p /var/www/conf.d

if [ ! -f /var/www/html/db/local.php ]
then
  cp /local.php /var/www/html/db/
  chown `id -u -n` /var/www/html/db/local.php
fi
if [ ! -f /var/www/html/lib/test/local.php ]
then
  cp /local_test.php /var/www/html/lib/test/local.php
  chown `id -u -n` /var/www/html/lib/test/local.php
fi
